import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { EstimatesPage } from '../pages/estimates/estimates';
import { ClientsPage } from '../pages/clients/clients';
import { EstimatePage } from '../pages/estimate/estimate';
import { EditEstimatePage } from '../pages/edit-estimate/edit-estimate';
import { ClientPage } from '../pages/client/client';
import { EditClientPage } from '../pages/edit-client/edit-client';
import { NewEstimatePage } from '../pages/new-estimate/new-estimate';
import { EstimatesService } from '../services/estimates';
import { HttpModule } from '@angular/http';
import { AppStore } from '../providers/app-store';
import { AuthService } from '../services/auth';
import { SignInPage } from '../pages/signin/signin';
import { SignUpPage } from '../pages/signup/signup';
import { IonicStorageModule } from '@ionic/storage';
import { TakeoffPage } from '../pages/takeoff/takeoff';
import { TakeoffSummaryPage } from '../pages/takeoff-summary/takeoff-summary';
import { InteriorTakeoffPage } from '../pages/interior-takeoff/interior-takeoff';
import { ExteriorPreviewPage } from '../pages/exterior-preview/exterior-preview';
import { ExteriorTakeoffPage } from '../pages/exterior-takeoff/exterior-takeoff';
import { RoomPage } from '../pages/interior-takeoff/room/room';
import { RoomPreviewPage } from '../pages/interior-preview/room-preview/room-preview';
import { SidePage } from '../pages/exterior-takeoff/side/side';
import { TakeoffService } from '../services/takeoff';
import { RoomItemPage } from '../pages/interior-takeoff/room-item/room-item';
import { ItemPreviewPage } from '../pages/interior-preview/item-preview/item-preview';
import { ClientsService } from '../services/clients';
import { TakeoffPreviewPage } from '../pages/takeoff-preview/takeoff-preview';
import { InteriorPreviewPage } from '../pages/interior-preview/interior-preview';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    ClientsPage,
    EstimatesPage,
    EstimatePage,
    ClientPage,
    NewEstimatePage,
    EditEstimatePage,
    SignInPage,
    SignUpPage,
    TakeoffPage,
    TakeoffSummaryPage,
    InteriorTakeoffPage,
    ExteriorTakeoffPage,
    RoomPage,
    RoomPreviewPage,
    SidePage,
    RoomItemPage,
    ItemPreviewPage,
    EditClientPage,
    TakeoffPreviewPage,
    InteriorPreviewPage,
    ExteriorPreviewPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    ClientsPage,
    EstimatesPage,
    EstimatePage,
    ClientPage,
    NewEstimatePage,
    EditEstimatePage,
    SignInPage,
    SignUpPage,
    TakeoffPage,
    TakeoffSummaryPage,
    InteriorTakeoffPage,
    ExteriorTakeoffPage,
    RoomPage,
    RoomPreviewPage,
    SidePage,
    RoomItemPage,
    ItemPreviewPage,
    EditClientPage,
    TakeoffPreviewPage,
    InteriorPreviewPage,
    ExteriorPreviewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppStore,
    EstimatesService,
    AuthService,
    TakeoffService,
    ClientsService
  ]
})
export class AppModule {}
