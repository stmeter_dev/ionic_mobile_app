import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { SignInPage } from '../pages/signin/signin';
import { SignUpPage } from '../pages/signup/signup';
import { AuthService } from '../services/auth';
import { AppStore } from '../providers/app-store';
import { User } from '../models/user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  signInPage:any = SignInPage;
  signUpPage:any = SignUpPage;
  isAuth: boolean = false
  current_user: User

  @ViewChild('nav') nav: NavController

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    private store: AppStore,
    private authService: AuthService) {

    this.authService.userState.subscribe(
      (user: User) => {
        if(user) {
          this.isAuth = true
          this.rootPage = TabsPage
          console.log('in app', user)
          //fetch all rates from API
          this.store.headers.set('Authorization', user.authToken)
          this.store.headers.set('X-Account', user.account)
          this.store.fetchAllRates()
        } else {
          this.isAuth = false
          this.rootPage = SignInPage
        }
      }
    )

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoad(page: any) {
    this.nav.setRoot(page)
    this.menuCtrl.close()
  }

  onSignOut() {
    this.authService.signout()
    this.menuCtrl.close()
    this.nav.setRoot(SignInPage)
  }

}
