import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import { Estimate } from '../models/estimate';

@Injectable()
export class AppStore {
  apiUrl = 'http://localhost:3000/v1/'
  headers = new Headers({'Content-Type': 'application/json'})
  store: {name: string, data: any}[] = []
  storeState = new Subject<{name: string, data: any}[]>()

  estimates: Estimate[] = []

  constructor(
    private http: Http,
    private storage: Storage
  ) {}

  addItem(item: {name: string, data: any}) {
    this.store.push(item)
    this.saveStore()
    this.storeState.next(this.store)
    console.log('store new item: ',this.store)
  }

  findItemByName(name: string) {
    let item = this.store.find((item: {name: string, data: any}) => {
      return item.name === name
    })
    return item
  }

  updateItem(newItem: {name: string, data: any}) {
    let item = this.findItemByName(newItem.name)
    item.data = newItem.data
    this.storeState.next(this.store)
  }

  removeItem(name: string) {
    let item = this.findItemByName(name)
    let index = this.store.indexOf(item)
    if(index > -1) {
      this.store.splice(index, 1)
    } else {
      console.log('item not found')
    }
    this.saveStore()
    this.storeState.next(this.store)
    console.log(this.store)
  }

  fetchAllRates() {
    return Observable.forkJoin(
      this.http.get(this.apiUrl + 'global_rates', { headers: this.headers})
      .map((res:Response) => res.json().global_rates),
      this.http.get(this.apiUrl + 'materials', { headers: this.headers})
      .map((res:Response) => res.json().materials),
      this.http.get(this.apiUrl + 'rates', { headers: this.headers})
      .map((res:Response) => res.json().rates),
      this.http.get(this.apiUrl + 'rate_categories', { headers: this.headers})
      .map((res:Response) => res.json().rate_categories)
    )
    .subscribe(
      data => {
        this.addItem({name: 'globalRates', data: data[0]})
        this.addItem({name: 'materials', data: data[1]})
        this.addItem({name: 'rates', data: data[2]})
        this.addItem({name: 'rateCategories', data: data[3]})
      },
      error => console.log(error)
    )
  }

  saveStore() {
    this.storage.set('AppStore', JSON.stringify(this.store))
  }

}
