export interface ProdRate {
  id: number;
  name: string;
  rate: number;
  unit: string;
  category: string;
}