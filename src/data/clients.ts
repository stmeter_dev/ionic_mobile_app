export default [
    {
      "id": 19,
      "first_name": "Mike",
      "last_name": "Kelly",
      "email": "mike.kelly@gmail.com",
      "phone_number": "342-098-3487",
      "full_name": "Mike Kelly"
    },
    {
      "id": 20,
      "first_name": "Khalid",
      "last_name": "Charaa",
      "email": "k.charaa@gmail.com",
      "phone_number": "654-234-8764",
      "full_name": "Khalid Charaa"
    }
]