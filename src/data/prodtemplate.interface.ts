import { ProdRate } from './prodrate.interface';

export interface ProdTemplate {
  id: number;
  name: string;
  active: boolean;
  production_rates: ProdRate[];
}