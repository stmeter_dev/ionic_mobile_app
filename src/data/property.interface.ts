import { Client } from './client.interface';

export interface Property {
  id: number;
  addr_line1: string;
  addr_line2: string;
  zipcode: string;
  city: string;
  state?: string;
  address?: string;
  client: Client;
}