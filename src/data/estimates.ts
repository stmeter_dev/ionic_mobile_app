export default  [
    {
      "id": 20,
      "start_time": "2017-03-20T09:15:00.000Z",
      "end_time": null,
      "work_description": "Interior and Exterior Painting",
      "status": "Pending",
      "work_type": "RES_INT_EXT",
      "property": {
        "id": 18,
        "addr_line1": "799 E Dragram Suite 5A",
        "addr_line2": "",
        "zipcode": "85705",
        "city": "Tucson",
        "state": "AZ",
        "address": "799 E Dragram Suite 5A",
        "client": {
          "id": 20,
          "first_name": "Khalid",
          "last_name": "Charaa",
          "email": "k.charaa@gmail.com",
          "phone_number": "654-234-8764",
          "full_name": "Khalid Charaa"
        }
      }
    },
    {
      "id": 21,
      "start_time": "2017-03-25T09:30:00.000Z",
      "end_time": "2017-03-25T11:30:00.000Z",
      "work_description": "Interior and Exterior Painting",
      "status": "Completed",
      "work_type": "RES_INT_EXT",
      "property": {
        "id": 17,
        "addr_line1": "239 C California Suite HJ",
        "addr_line2": "",
        "zipcode": "43456",
        "city": "Kansas",
        "state": "CA",
        "address": "239 C California Suite HJ",
        "client": {
          "id": 10,
          "first_name": "Mike",
          "last_name": "Kelly",
          "email": "mike.kelly@gmail.com",
          "phone_number": "342-098-3487",
          "full_name": "Mike Kelly"
        }
      }
    }
]