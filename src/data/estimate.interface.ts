import { Property } from './property.interface';

export interface Estimate {
  id: number;
  start_time: any;
  end_time: any;
  work_description: string;
  status: string;
  work_type: string;
  property: Property;
}