export default [
    {
      "id": 10,
      "name": "Default Template",
      "active": true,
      "production_rates": [
        {
          "id": 17,
          "name": "I-Window",
          "rate": 90,
          "rate_type": "per Item",
          "category": "INT"
        },
        {
          "id": 18,
          "name": "I-Door",
          "rate": 110,
          "rate_type": "per Item",
          "category": "INT"
        },
        {
          "id": 19,
          "name": "Trim",
          "rate": 66,
          "rate_type": "per LFT",
          "category": "INT"
        },
        {
          "id": 20,
          "name": "X-Door",
          "rate": 120,
          "rate_type": "per Item",
          "category": "EXT"
        },
        {
          "id": 21,
          "name": "X-Window",
          "rate": 90,
          "rate_type": "per Item",
          "category": "EXT"
        }
      ]
    }
]