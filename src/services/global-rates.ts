import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class GlobalRatesService {
  constructor() { }

  toFormGroup(rates: {name: string, value: number}[] ) {
    let group: any = {};

    rates.forEach(rate => {
      group[rate.name] = new FormControl(rate.value, Validators.required)
    });
    return new FormGroup(group);
  }
}
