import { Injectable } from '@angular/core';
import { Estimate } from '../models/estimate';
import { Client } from '../models/client';
import { Headers, Http, Response } from '@angular/http';
import { AppStore } from '../providers/app-store';
import { AuthService } from './auth';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class EstimatesService {
  estimates: Estimate[] = []
  private apiUrl
  private headers = new Headers({
    'Content-Type': 'application/json'
  })
  estimatesState = new Subject<Estimate[]>()
  estimateUpdated = new Subject<Estimate>()
  estimateAdded = new Subject<Estimate>()
  constructor(
    private store: AppStore,
    private http: Http,
    private authService: AuthService) {
      this.apiUrl = this.store.apiUrl
      this.headers = this.store.headers
    }

  addEstimate(estimate: Estimate) {
    this.estimates.push(estimate)
  }

  authorizeCalendar() {
    return this.http.get(this.apiUrl + 'oauth2calendarcallback')
    .map((res: Response) => res.json())
  }

  createClientEstimate(cData: any, eData: any): Observable<Estimate> {
    //this.setHeaders()
    return this.http.post(
      this.apiUrl + 'clients',
      JSON.stringify(cData),
      { headers: this.headers }
    )
    .map((res: Response) => res.json().client)
    .mergeMap((client: Client) => {
      eData.estimate.property_id = client.properties[0].id
      return this.http.post(
        this.apiUrl + 'estimates',
        JSON.stringify(eData),
        { headers: this.headers }
      )
      .map((resp: Response) => new Estimate(resp.json().estimate))
    })
    .catch(this.handleError)
  }

  newClientEstimate(data: any): Observable<any> {
    //add the case when user fetching fails!
    return this.http.post(
      this.apiUrl + 'clients/',
      JSON.stringify(data),
      { headers: this.headers }
    )
    .map((resp: Response) => resp.json().client)
    .catch(this.handleError)
  }

  removeEstimate(estimate: Estimate, index: number) {
    return this.http.delete(
      this.apiUrl + 'estimates/' + estimate.id,
      { headers: this.headers}
    )
    .map(
      () => {
        this.estimates.splice(index, 1)
        this.estimatesState.next(this.estimates)
      }
    )
  }

  fetchEstimates(): Observable<Estimate[]> {
    return this.http.get(
      this.apiUrl + 'estimates/',
      { headers: this.headers }
    )
    .map(this.mapEstimates)
    .catch(this.handleError)
  }

  updateEstimate(id: number, estimateData: any): Observable<Estimate>  {
    console.log('estimate called', id)
    console.log('in update estimate: ',this.estimates)
    const estimate = this.estimates.find((e: Estimate) => {
      return e.id === id
    })
    console.log('found? ', estimate)
    if(estimate) {
      console.log('estimate found...')
      estimate.start_time = estimateData.start_time || estimate.start_time
      estimate.work_type = estimateData.work_type || estimate.work_type
      estimate.work_description = estimateData.work_description || estimate.work_description
      estimate.status = estimateData.status || estimate.status
      estimate.end_time = estimateData.end_time || estimate.end_time

      return this.http.put(
        this.apiUrl + 'estimates/' + id,
        JSON.stringify({"estimate": {
          "start_time": estimate.start_time,
          "work_type": estimate.work_type,
          "work_description": estimate.work_description,
          "status": estimate.status
          }}),
        { headers: this.headers}
      )
      .map((resp: Response) => resp.json().estimate)
      .catch(this.handleError)
    }
  }

  getEstimates(): Estimate[] {
    return this.estimates.slice()
  }

  private mapEstimates(resp: Response) {
    let data = resp.json().estimates
    let estimates: Estimate[] = []
    data.forEach(estimate => {
      estimates.push(new Estimate(estimate))
    })
    this.estimates = estimates
    console.log('in map estimtes', this.estimates)
    return estimates
  }

  private handleError(error: any): Promise<any> {
    console.log(error)
    return Promise.reject(error)
 }

}
