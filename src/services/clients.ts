import { Injectable } from '@angular/core';
import { Estimate } from '../models/estimate';
import { Headers, Http, Response } from '@angular/http';
import { AppStore } from '../providers/app-store';
import { AuthService } from './auth';
import { User } from '../models/user';
import { Client } from '../models/client';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ClientsService {
  clients: Client[] = []
  client: Client
  private apiUrl
  private headers

  constructor(
    private store: AppStore,
    private http: Http,
    private authService: AuthService) {
      this.apiUrl = this.store.apiUrl
      this.headers = this.store.headers
    }

  // fetchClients(user: User) {
  //   this.headers.set('Authorization', user.authToken)
  //   this.headers.set('X-Account', user.account)
  //
  //   return this.http.get(
  //     this.apiUrl + 'clients/',
  //     { headers: this.headers }
  //   )
  //   .toPromise()
  //   .then(res => {
  //     const list = []
  //     res.json().clients.forEach((client: Client) => {
  //       list.push(new Client(client))
  //     })
  //     this.clients = list
  //     return this.clients
  //   })
  //   .catch(err => console.log(err))
  // }

  fetchClients(): Observable<Client[]> {
    return this.http.get(
      this.apiUrl + 'clients/',
      { headers: this.headers }
    )
    .map(this.mapClients)
    .catch(this.handleError)
  }

  fetchClient(id: number): Observable<Client> {
    return this.http.get(
      this.apiUrl + '/clients/' + id,
      { headers: this.headers }
    )
    .map((resp: Response) => {
      return new Client(resp.json().client)
    })
    .catch(this.handleError)
  }

  private mapClients(resp: Response) {
    let data = resp.json().clients
    let clients: Client[] = []
    data.forEach(client => {
      clients.push(new Client(client))
    })
    return clients
  }

  private handleError(error: any): Promise<any> {
    console.log(error)
    return Promise.reject(error)
 }
}
