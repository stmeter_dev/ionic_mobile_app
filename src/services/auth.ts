import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AppStore } from '../providers/app-store';
import { User } from '../models/user';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment-timezone';

@Injectable()
export class AuthService {
  private apiUrl = 'http://localhost:3000/v1/'
  private headers = new Headers({'Content-Type': 'application/json'})
  current_user: User
  userState = new Subject<User>()
  timeZone = moment.tz.guess()
  constructor(
    private http: Http,
    private storage: Storage,
    private store: AppStore
  ) {}

  signup(email: string, password: string, password_confirmation: string,
    account: string): Observable<any> {
    return this.http.post(
      this.apiUrl + 'accounts',
      JSON.stringify({"account": {
        "name": account,
        "time_zone": this.timeZone,
        "owner_attributes": {
          "email": email,
          "password": password,
          "password_confirmation": password_confirmation,
          "time_zone": this.timeZone
        }
      }}),
      { headers: this.headers }
    )
  }

  signin(email: string, password: string): Observable<User> {
    return this.http.post(
      this.apiUrl + 'sessions',
      JSON.stringify({session: {
        email: email,
        password: password
      }}),
      { headers: this.headers}
    )
    .map(this.mapUser)
  }

  signout(): void {
    let user = this.store.findItemByName('current_user').data
    this.headers.set('X-Account', user.account)
    this.http.delete(
      this.apiUrl + 'sessions/' + user.authToken,
      { headers: this.headers}
    )
    .map((resp: Response) => {
      this.userState.next(null)
      this.store.removeItem('current_user')
      this.store.headers.delete('X-Account')
      this.store.headers.delete('Authorization')
    })
    .subscribe()
  }

  private mapUser(resp: Response) {
    let data = resp.json()
    let user = new User(data.user.name,
      data.user.email, data.account.name,
      data.user.auth_token, true)
    return user
  }

}
