import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/mergeMap';
import { AppStore } from '../providers/app-store';
import { Takeoff } from '../models/takeoff';
import { User } from '../models/user';
import { Rate } from '../models/rate';
import { RateCategory } from '../models/rate-category';
import { GlobalRate } from '../models/global-rate';
import { Material } from '../models/material';
import { Room } from '../models/room';
import { Side } from '../models/side';
import { Item } from '../models/item';
import { AuthService } from './auth';

@Injectable()
export class TakeoffService {
  private apiUrl = 'http://localhost:3000/v1/'
  headers = new Headers({'Content-Type': 'application/json'})
  globalRates: GlobalRate[] = []
  rates: Rate[] = []
  rateCategories: RateCategory[] = []
  materials: Material[] = []
  jobMaterial: Material
  roomsList: Room[] = []
  roomChange = new Subject<Room>()
  takeoffState = new Subject<Takeoff>()
  materialChange = new Subject<Material>()
  defaultSideNames: string[] = ['Front','Right', 'Back', 'Left']
  sidesList: Side[] = []
  sideItems: Item[] = []

  constructor(
    private http: Http,
    private storage: Storage,
    private store: AppStore,
    private authService: AuthService) {
      this.headers = this.store.headers
    }

  fetchAll() {
    return Observable.forkJoin(
      this.http.get(this.apiUrl + 'global_rates', { headers: this.headers})
      .map((res:Response) => res.json().global_rates),
      this.http.get(this.apiUrl + 'materials', { headers: this.headers})
      .map((res:Response) => res.json().materials),
      this.http.get(this.apiUrl + 'rates', { headers: this.headers})
      .map((res:Response) => res.json().rates),
      this.http.get(this.apiUrl + 'rate_categories', { headers: this.headers})
      .map((res:Response) => res.json().rate_categories)
    )
    .subscribe(
      data => {
        this.globalRates = data[0]
        this.materials = data[1]
        this.rates = data[2]
        this.rateCategories = data[3]
        console.log(data)
      },
      error => console.log(error)
    )
  }

  //splict the create takeoff to two functions: first create and update takeoff then create rooms for property

  createTakeoff(estimateId: number, takeoff: Takeoff): Observable<Takeoff> {
    return this.http.post(
      this.apiUrl + 'estimates/' + estimateId + '/takeoffs',
      JSON.stringify(takeoff),
      { headers: this.headers }
    )
    .map((resp: Response) => resp.json().takeoff)
    .mergeMap((takeoff: Takeoff) => {
      takeoff.saved = true
      return this.http.put(
        this.apiUrl + 'estimates/' + estimateId + '/takeoffs/' + takeoff.id,
        JSON.stringify({
          takeoff: {
            saved: true,
            status: "Completed"
          }
        }),
        { headers: this.headers }
      )
      .map((resp: Response) => resp.json().takeoff)
    })
    .catch(this.handleError)
  }

  updateTakeoff(estimateId: number, takeoffId: number, updatedTakeoff: Takeoff): Observable<Takeoff> {
    return this.http.put(
      this.apiUrl + 'estimates/' + estimateId + '/takeoffs/' + takeoffId,
      JSON.stringify({
        takeoff: updatedTakeoff
      }),
      {headers: this.headers}
    )
    .map((resp: Response) => resp.json().takeoff)
    .catch(this.handleError)
  }

  createRooms(propertyId: number, rooms: Room[]): Observable<Room[]> {
    return this.http.post(
      this.apiUrl + 'properties/' + propertyId + '/rooms',
      JSON.stringify({
        room: rooms
      }),
      { headers: this.headers }
    )
    .map((resp: Response) => {
      return resp.json().rooms
    })
    .catch(this.handleError)
  }

  //Old create takeoff with mergeMap, it is returning rooms instead of takeoff

  // createTakeoff(estimateId: number, propertyId: number, takeoff: Takeoff): Observable<Takeoff> {
  //   console.log('in create takeoff ',takeoff.rooms)
  //   return this.http.post(
  //     this.apiUrl + 'estimates/' + estimateId + '/takeoffs',
  //     JSON.stringify(takeoff),
  //     { headers: this.headers }
  //   )
  //   .map((resp: Response) => {
  //     return resp.json().takeoff
  //   })
  //   .mergeMap(() => {
  //     return this.http.post(
  //       this.apiUrl + 'properties/' + propertyId + '/rooms',
  //       JSON.stringify(
  //         { room: takeoff.rooms }
  //       ),
  //       {headers: this.headers}
  //     )
  //     .map((resp: Response) => resp.json())
  //   })
  //   .catch(this.handleError)
  // }
  //if we know the takeoff id we use this method
  // fetchTakeoff(estimateID: number, takeoffId: number): Observable<Takeoff> {
  //   return this.http.get(
  //     this.apiUrl + 'estimates/' + estimateID + '/takeoffs/' + takeoffId,
  //     {headers: this.headers  }
  //   )
  //   .map((resp: Response) => {
  //     return resp.json().takeoff
  //   })
  //   .catch(this.handleError)
  // }

  //since one estimate can have only one takeoff, we can list all takeoffs (takeoffs#index)
  fetchTakeoff(estimateID: number): Observable<Takeoff> {
    return this.http.get(
      this.apiUrl + 'estimates/' + estimateID + '/takeoffs',
      {headers: this.headers  }
    )
    .map((resp: Response) => {
      return resp.json().takeoff
    })
    .catch(this.handleError)
  }

  setSides() {
    this.defaultSideNames.forEach((name: string) => {
      this.sidesList.push(new Side(name, 50, 20, 1, this.sideItems))
    })
    console.log(this.sidesList)
  }

  removeSide(index: number) {
    this.sidesList.splice(index, 1)
  }

  getSides() {
    return this.sidesList.slice()
  }

  saveRooms() {
    let rooms = JSON.stringify(this.roomsList)
    this.storage.set('rooms', rooms)
  }

  private handleError (error: Response | any) {
    let errMsg: string = 'Operation failed!!';
    return Observable.throw(errMsg);
  }

}
