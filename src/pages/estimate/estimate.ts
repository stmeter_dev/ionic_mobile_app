import { Component, OnInit } from '@angular/core';
import { NavParams, ActionSheetController, NavController } from 'ionic-angular';
import { TakeoffService } from '../../services/takeoff';
import { Estimate } from '../../models/estimate';
import { ClientPage } from '../client/client';
import { TakeoffPage } from '../takeoff/takeoff';
import { Client } from '../../models/client';
import { Takeoff } from '../../models/takeoff';
import { TakeoffPreviewPage } from '../takeoff-preview/takeoff-preview';

@Component({
  selector: 'page-estimate',
  templateUrl: 'estimate.html'
})

export class EstimatePage implements OnInit {
  estimate: Estimate
  client: Client
  takeoff: Takeoff
  clientPage: any =  ClientPage;
  constructor(
    private navParams: NavParams,
    private navCtrl: NavController,
    private asCtrl: ActionSheetController,
    private toService: TakeoffService
  ) {}

  ngOnInit() {
    this.estimate = this.navParams.get('estimate')
    console.log(this.estimate)
    this.client = this.estimate.property.client
    this.takeoff = this.estimate.takeoff
  }

  ionViewWillEnter() {
    //this.setEstimateStatus()
  }

  onLoadTakeoff() {
    this.toService.fetchTakeoff(this.estimate.id)
    .subscribe(
      (takeoff: Takeoff) => {
        this.navCtrl.push(TakeoffPreviewPage, {takeoff: takeoff, client: this.client})
      }
    )
  }

  onStartTakeoff(estimate: Estimate) {
    this.takeoff.start = new Date()
    this.takeoff.status = 'Ongoing'
    this.navCtrl.push(TakeoffPage, {estimate: this.estimate})
  }

  onViewTakeoff() {
    this.navCtrl.push(TakeoffPage, {estimate: this.estimate})
  }

  getEstimateStatus() {
    if(this.estimate.status === 'Takeoff Completed') {
      return true
    } else {
      return false
    }
  }

  setEstimateStatus() {
    let takeoffStatus = this.takeoff.status
    switch(takeoffStatus) {
      case 'Pending':
        this.estimate.status = 'Pending'
        break
      case 'Ongoing':
        this.estimate.status = 'Takeoff Ongoing'
        break
      case 'Completed':
        this.estimate.status = 'Takeoff Completed'
        this.estimate.end_time = this.takeoff.end
        break
    }
  }

  onLoadClient(client: Client) {
    this.navCtrl.push(ClientPage, {client: client})
  }

}
