import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AppStore } from '../../providers/app-store';
import { TakeoffService } from '../../services/takeoff';
import { Client } from '../../models/client';
import { Estimate } from '../../models/estimate';
import { Takeoff } from '../../models/takeoff';
import { Room } from '../../models/room';
import { GlobalRate } from '../../models/global-rate';
import { TakeoffSummaryPage } from '../takeoff-summary/takeoff-summary';
import { InteriorTakeoffPage } from '../interior-takeoff/interior-takeoff';
import { ExteriorTakeoffPage } from '../exterior-takeoff/exterior-takeoff';
import { InteriorPreviewPage } from '../interior-preview/interior-preview';

@Component({
  selector: 'page-takeoff-preview',
  templateUrl: 'takeoff-preview.html'
})
export class TakeoffPreviewPage implements OnInit {
  takeoff: Takeoff
  client: Client
  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private alrtCtrl: AlertController,
              private store: AppStore,
              private toService: TakeoffService
              ) {
  }

  ngOnInit() {
    this.takeoff = this.navParams.get('takeoff')
    this.client = this.navParams.get('client')
  }

  ionViewWillEnter() {

  }

  onPreviewSummary() {
    this.navCtrl.push(TakeoffSummaryPage, {takeoff: this.takeoff, client: this.client})
  }

  onPreviewInterior() {
    this.navCtrl.push(InteriorPreviewPage, {takeoff: this.takeoff})
  }

  onPreviewExterior() {
    //this.navCtrl.push(ExteriorTakeoffPage)
  }

  onLoadTakeoffSummary() {
    //this.navCtrl.push(TakeoffSummaryPage, {jobType: this.jobType})
  }


}
