import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { Client } from '../../models/client';
import { Estimate } from '../../models/estimate';
import { ClientPage } from '../client/client';
import { ClientsService } from '../../services/clients';
import { AuthService } from '../../services/auth';

@Component({
  selector: 'page-clients',
  templateUrl: 'clients.html'
})
export class ClientsPage implements OnInit {
  clients: Client[] = []

  constructor(private navCtrl: NavController,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private clientsService: ClientsService,
              private authService: AuthService
              ) {
  }

  // ngOnInit() {
  //   const loading = this.loadingCtrl.create({
  //     content: 'Loading clients...'
  //   })
  //   loading.present()
  //   this.authService.getCurrentUser()
  //   .then(user => {
  //     if(user) {
  //       this.clientsService.fetchClients(user)
  //       .then((list: Client[]) => {
  //         loading.dismiss()
  //         if(list) {
  //           this.clients = list
  //         } else {
  //           this.clients = []
  //         }
  //       })
  //       .catch(error => {
  //         console.log(error)
  //         loading.dismiss()
  //         const alert = this.alertCtrl.create({
  //               title: 'Ooops!',
  //               message: error.errors,
  //               buttons: ['OK']
  //             })
  //         alert.present()
  //       })
  //     }
  //   })
  // }

  ngOnInit() {
    const loading = this.loadingCtrl.create({
      content: 'Loading clients...'
    })
    loading.present()
    this.clientsService.fetchClients().subscribe(
      (clients: Client[]) => {
        this.clients = clients
        loading.dismiss()
      },
      (error: Error) => {
        loading.dismiss()
        const alert = this.alertCtrl.create({
          title: 'Ooops! Something went wrong...',
          message: error.message,
          buttons: ['OK']
        })
        alert.present()
      }
    )
  }

  onNewClient() {
    //this.navCtrl.push(NewClientPage)
  }

  onLoadClient(client: Client, index: number) {
    this.navCtrl.push(ClientPage, {client: client, index: index})
  }

  onCallClient(client: Client) {
    // this.callNumber.callNumber(client.cell, true)
    // .then(() => console.log('Launched dialer!'))
    // .catch(() => console.log('Error launching dialer'));
  }

  onDeleteClient(client: Client, index: number) {

  }
}
