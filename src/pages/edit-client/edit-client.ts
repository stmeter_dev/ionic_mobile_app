import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { NavController, NavParams } from 'ionic-angular';
import { Client } from '../../models/client';
import { Property } from '../../models/property';

@Component({
  selector: 'page-edit-client',
  templateUrl: 'edit-client.html'
})
export class EditClientPage implements OnInit {
  clientParams: any
  client: Client;
  property: Property
  index: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              ) {
  }

  ngOnInit() {
    this.client = this.navParams.get('client')
    this.index = this.navParams.get('index')
  }

  onSaveClient(form: NgForm){
    this.clientParams = {
      first_name: form.value.firstName,
      last_name: form.value.lastName,
      cell: form.value.cell,
      phone: form.value.phone,
      email: form.value.email
    }
    // this.client = new Client(this.clientParams)
    // this.property = new Property(form.value.address, this.client)
  }

}
