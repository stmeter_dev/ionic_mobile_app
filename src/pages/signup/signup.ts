import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth';
import { SignInPage } from '../signin/signin';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})

export class SignUpPage {
  constructor(
    private authService: AuthService,
    private navCtrl: NavController
  ) {}

  onSignup(form: NgForm) {
    this.authService.signup(form.value.email, form.value.password,
    form.value.password_confirmation, form.value.account)
    .subscribe(
      (data) => {
        this.navCtrl.push(SignInPage)
      }
    )

  }
}
