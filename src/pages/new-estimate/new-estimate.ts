import { Component, OnInit } from '@angular/core';
import { NavParams, ViewController, LoadingController } from 'ionic-angular';
import { Headers, Http, Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { Estimate } from '../../models/estimate';
import { Client } from '../../models/client';
import { Property } from '../../models/property';
import { EstimatesService } from '../../services/estimates';

@Component({
  selector: 'page-new-estimate',
  templateUrl: 'new-estimate.html'
})

export class NewEstimatePage {
  newEstimateData: any
  clientData: any
  estimateData: any
  jobTypes: String[] = ['Interior', 'Exterior', 'Interior & Exterior']

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private estimatesService: EstimatesService,
    http: Http
  ) {}

  onAddEstimate(form: NgForm) {
    this.constructData(form)

    const loading = this.loadingCtrl.create({
      content: 'Processing, please wait...'
    })

    this.estimatesService.authorizeCalendar().subscribe(
      (res) => {
        if(res.authorized === 'NOK') {
          window.location.href = res.redirect_to
        } else {
          loading.present()
          this.estimatesService.createClientEstimate(this.clientData, this.estimateData)
          .subscribe(
            (estimate: Estimate) => {
              loading.dismiss()
              this.estimatesService.estimateAdded.next(estimate)
              this.estimatesService.addEstimate(new Estimate(estimate))
              this.estimatesService.estimatesState.next(this.estimatesService.getEstimates())
              this.viewCtrl.dismiss()
            }
          )
        }
      }
    )

  }

  private constructData(form: NgForm) {
    console.log(form.value.start_time)
    this.clientData = {
      "client": {
        first_name: form.value.firstName,
        last_name: form.value.lastName,
        email: form.value.email,
        phone_number: form.value.phone_number,
        "properties_attributes": [{
          addr_line1: form.value.addr_line1,
          addr_line2: form.value.addr_line2,
          zipcode: form.value.zipcode,
          city: form.value.city
        }]
      }
    }

    this.estimateData = {
      "estimate": {
        property_id: 0,
        status: 'Pending',
        start_time: form.value.start_time,
        work_type: form.value.work_type,
        work_description: form.value.work_description
      }
    }

    this.storeData()
  }

  private storeData() {
    localStorage.setItem('clientData', JSON.stringify(this.clientData))
    localStorage.setItem('estimateData', JSON.stringify(this.estimateData))
  }

}
