import { Component, OnInit } from '@angular/core';
import { NavController, NavParams,
  AlertController, ActionSheetController,
  ModalController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AppStore } from '../../../providers/app-store';
import { TakeoffService } from '../../../services/takeoff';
import { Room } from '../../../models/room';
import { Item } from '../../../models/item';
import { Rate } from '../../../models/rate';
import { RateCategory } from '../../../models/rate-category';
import { GlobalRate } from '../../../models/global-rate';
import { Material } from '../../../models/material';
import { Measure } from '../../../models/measure';
import { RoomItemPage } from '../room-item/room-item';

@Component({
  selector: 'page-room',
  templateUrl: 'room.html'
})
export class RoomPage implements OnInit {
  room: Room
  item: Item
  eMode: string
  gRates: GlobalRate[] = []
  rates: Rate[] = []
  rateCategories: RateCategory[] = []
  measures: Measure[] = []
  materials: Material[] = []
  defaultMaterial: Material
  roomItems: Item[] = []
  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private asCtrl: ActionSheetController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private store: AppStore,
    private toService: TakeoffService
  ) {}

  ngOnInit() {
    let quantity = 0
    this.gRates = this.store.findItemByName('globalRates').data
    this.materials = this.store.findItemByName('materials').data
    this.room = this.navParams.get('room')
    this.roomItems = this.room.room_features_attributes
    this.defaultMaterial = this.room.material_type
    this.rateCategories = this.store.findItemByName('rateCategories').data.filter(
      (cat: RateCategory) => {
        return cat.inout === "IN"
      }
    )

    this.rates = this.store.findItemByName('rates').data.filter(
      (rate: Rate) => {
        return rate.rate_category.inout === "IN"
      }
    )

    this.rateCategories.forEach((category: RateCategory) => {
      console.log(category.id)
      let itemLength = 0
      let itemWidth = 0
      let item = this.findItem(category.name)
      if(typeof item === 'undefined') {
        if(category.name !== 'Walls' && category.name !== 'Ceiling') {
          if(category.unit === "SQF/Hr") {
            itemLength = 1
            itemWidth = 1
          } else if (category.unit === 'LF/Hr') {
            itemLength = 1
            itemWidth = 0
          } else if (category.unit === 'Hr') {
            itemLength = 0
            itemWidth = 0
            quantity = 1
          }
        }
        //since the material type is not known yet, will just set it's id to -1 for now
        let material_id = -1
        if(this.defaultMaterial) {
          material_id = this.defaultMaterial.id
        }
        this.item = new Item(category.name, quantity, false, 0, 1, 0, 0,
          this.defaultMaterial, material_id, itemLength, itemWidth, '', '', category, category.id)
        this.roomItems.push(this.item)
      }
    })
    this.room.room_features_attributes = this.roomItems
  }

  findItem(name: string): Item {
    return this.roomItems.find((item: Item) => {
      return item.name === name
    })
  }

  setWorkHours() {
    return this.room.room_features_attributes.reduce((totalHours: number, item: Item) => {
      return Math.round((totalHours + item.hours) * 100) / 100
    }, 0)
  }

  setTotalMaterial() {
    return this.room.room_features_attributes.reduce((totalMaterial: number, item: Item) => {
      //return totalMaterial + item.material_quantity
      return Math.round((totalMaterial + item.material_quantity) * 100) / 100
    }, 0)
  }

  setRoomMaterialsCost() {
    let materialCost = this.room.material_type.cost
    console.log(materialCost)
    return Math.round(this.room.material_quantity * materialCost * 100) / 100
  }

  setRoomLaborCost() {
    let labor: GlobalRate = this.gRates.filter((rate: GlobalRate) => {
      return rate.name === 'labor'
    }).pop()

    return Math.round(this.room.hours * labor.value * 100) / 100
  }

  onRoomChange(event) {
    this.toService.roomChange.next(this.room)
  }

  addItem(name: string) {
    // this.item = new Item(name, 1, 0, 1, 1, 1, '', '')
    // this.roomItems.push(this.item)
    // this.room.items = this.roomItems
  }

  itemQuantity(rate: Rate) {

  }

  onRemoveItem(index: number) {
    this.roomItems.splice(index, 1)
    this.room.room_features_attributes = this.roomItems
  }

  onAddItem() {
    let prompt = this.alertCtrl.create({
      title: 'New Item Name',
      message: "Enter a name for the new item",
      inputs: [
        {
          name: 'name',
          placeholder: 'New item name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {}
        },
        {
          text: 'Add',
          handler: data => {
            this.addItem(data.name)
          }
        }
      ]
    })
    prompt.present()
  }

  onMaterialChange(selectedMaterial: Material) {
    this.room.material_type = selectedMaterial
    this.room.material_id = selectedMaterial.id
    this.room.room_features_attributes.forEach((item: Item) => {
      item.material_type = selectedMaterial
      item.material_id = selectedMaterial.id
    })
    this.toService.jobMaterial = selectedMaterial
    this.toService.materialChange.next(selectedMaterial)
  }

  setItemRate(item: Item, rate: Rate) {
    console.log(rate)
    item.rate = rate
    item.rate_id = rate.id
  }

  onLoadItem(item: Item) {
    if(!item.set) {
      const actionSheet = this.asCtrl.create({
        title: 'Select ' + item.name + ' type'
      })
      item.category.rates.forEach((rate: Rate) => {
        actionSheet.addButton({
          text: rate.name,
          handler: () => {
            this.setItemRate(item, rate)
            //this.navCtrl.push(RoomItemPage, {room: this.room, item: item})
            let itemModal = this.modalCtrl.create(RoomItemPage, {room: this.room, item: item})
            itemModal.present()
          }
        })
      })
      actionSheet.addButton({text: 'Cancel', role: 'cancel' })
      actionSheet.present()
    } else {
      this.navCtrl.push(RoomItemPage, {room: this.room, item: item})
    }
  }

  onSaveRoom(form: NgForm) {
    console.log(form.value.materialType)
    if(typeof form.value.materialType === 'undefined') {
      let alert = this.alertCtrl.create({
        title: 'Material type is missing!',
        message: 'Please select material',
        buttons: ['OK']
      })
      alert.present()
    } else {
      this.room.set = true
      this.room.hours = this.setWorkHours()
      this.room.material_quantity = this.setTotalMaterial()
      console.log('material in save room: ',this.room.material_quantity)
      this.room.labor_cost = this.setRoomLaborCost()
      this.room.material_cost = this.setRoomMaterialsCost()
      //save rooms to localstorage: bo be updated
      this.toService.saveRooms()
      this.navCtrl.pop()
    }

  }
}
