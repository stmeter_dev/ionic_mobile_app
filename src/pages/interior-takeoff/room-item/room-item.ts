import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';
import { TakeoffService } from '../../../services/takeoff';
import { AppStore } from '../../../providers/app-store';
import { RateCategory } from '../../../models/rate-category';
import { Rate } from '../../../models/rate';
import { Room } from '../../../models/room';
import { Item } from '../../../models/item';
import { Material } from '../../../models/material';

@Component({
  selector: 'page-room-item',
  templateUrl: 'room-item.html'
})
export class RoomItemPage implements OnInit {
  room: Room
  item: Item
  rates: Rate[] = []
  materials: Material[] = []
  itemLength: number = 0
  itemWidth: number = 0
  itemQuantity: number
  minItemLength: number
  roomSQF: number = 0
  coats: number
  constructor(//private navCtrl: NavController,
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private toService: TakeoffService,
    private store: AppStore
  ) {
  }

  ionViewWillEnter() {
    this.setHours()
    this.setMaterialQuantity()
    this.toService.roomChange.subscribe(
      (room: Room) => {
        this.room = room
        this.setHours()
        this.setMaterialQuantity()
      }
    )
  }

  ngOnInit() {
    // this.toService.roomChange.subscribe(
    //   (room: Room) => {
    //     this.room = room
    //     this.setHours()
    //     this.setMaterialQuantity()
    //   }
    // )

    this.materials = this.store.findItemByName('materials').data
    this.item = this.navParams.get('item')
    this.room = this.navParams.get('room')
    this.rates = this.item.category.rates
    this.itemLength = this.item.length
    this.itemWidth = this.item.width
    this.itemQuantity = this.item.quantity
    if(this.item.category.name === 'Doors' || this.item.category.name === 'Windows') {
      this.minItemLength = 1
    } else {
      this.minItemLength = (this.room.length + this.room.width) * 2
    }
  }

  setHours() {
    this.item.set = true
    let hours  = 0
    const L = this.room.length
    const W = this.room.width
    const H = this.room.height
    this.coats = this.item.coats
    let C1 = this.item.rate.first_coat
    let C2 = this.item.rate.second_coat

    if(this.coats === 1) {
      C2 = 0
    }

    if(this.item.category.name === 'Walls') {
      hours = this.setWallsHours(L, W, H, C1, C2)
    } else if (this.item.category.name === 'Ceiling') {
      hours = this.setCeilingHours(L, W, C1, C2)
    } else if (this.item.category.unit === 'SQF/Hr') {
      hours = this.setSQFHours(this.item.length, this.item.width, C1, C2)
    } else if (this.item.category.unit === 'LF/Hr') {
      let cat = this.item.category.name
      if(cat === 'Base' || cat === 'Crown' || cat === 'Trim') {
        if(this.item.length < (L + W) * 2) {
          this.item.length = (L + W) * 2
        }
      }
      hours = this.setLFHours(this.item.length, C1, C2)
    } else if (this.item.category.unit === 'Hr') {
      console.log('quantity is ',this.item.quantity)
      hours = this.item.quantity * (C1 + C2)
    }
    this.item.hours = Math.round((hours + this.item.prep) * 100) / 100
  }

  setWallsHours(L: number, W: number, H: number, C1: number, C2: number): number {
    let wallsWorkHours  = 0
    let wallsSQF = (L*H + W*H) * 2

    if(C2 === 0) {
      wallsWorkHours = wallsSQF/C1
    } else {
      wallsWorkHours = (wallsSQF/C1) + (wallsSQF/C2)
    }
    return wallsWorkHours
  }

  setWallsMaterial(L: number, W: number, H: number, coats: number, cov: number): number {
    let wallsMaterial  = 0
    let wallsSQF = (L*H + W*H) * 2 * coats
    wallsMaterial = wallsSQF/cov
    return wallsMaterial
  }

  setCeilingHours(L: number, W: number, C1: number, C2: number): number {
    let ceilingWorkHours  = 0
    let ceilingSQF = L*W

    if(C2 === 0) {
      ceilingWorkHours = ceilingSQF/C1
    } else {
      ceilingWorkHours = (ceilingSQF/C1) + (ceilingSQF/C2)
    }
    return ceilingWorkHours
  }

  setCeilingMaterial(L: number, W: number, coats: number, cov: number): number {
    let ceilingMaterial  = 0
    let ceilingSQF = L*W * coats
    ceilingMaterial = ceilingSQF/cov
    return ceilingMaterial
  }

  setSQFHours(L: number, W: number, C1: number, C2: number): number {
    let sQFWorkHours  = 0
    let SQF = L*W

    if(C2 === 0) {
      sQFWorkHours = SQF/C1
    } else {
      sQFWorkHours = (SQF/C1) + (SQF/C2)
    }
    return sQFWorkHours
  }

  setSQFMaterial(L: number, W: number, coats: number, cov: number): number {
    let sQFMaterial  = 0
    let SQF = L*W * coats
    sQFMaterial = SQF/cov
    return sQFMaterial
  }

  setLFHours(L: number, C1: number, C2: number): number {
    let lFWorkHours = 0

    if(C2 === 0) {
      lFWorkHours = L/C1
    } else {
      lFWorkHours = (L/C1) + (L/C2)
    }

    return lFWorkHours
  }

  setLFMaterial(L: number, coats: number, cov: number): number {
    let lFMaterial  = 0
    let LF = L * coats
    lFMaterial = LF/cov
    return lFMaterial
  }

  setMaterialQuantity() {
    const L = this.room.length
    const W = this.room.width
    const H = this.room.height
    let cov = this.item.material_type.cov
    let itemMaterial = 0

    if(this.item.category.name === 'Walls') {
      itemMaterial = this.setWallsMaterial(L, W, H, this.coats, cov)
    } else if (this.item.category.name === 'Ceiling') {
      itemMaterial = this.setCeilingMaterial(L, W, this.coats, cov)
    } else if (this.item.category.unit === 'SQF/Hr') {
      itemMaterial = this.setSQFMaterial(this.item.length, this.item.width, this.coats, cov)
    } else if (this.item.category.unit === 'LF/Hr') {
      itemMaterial = this.setLFMaterial(this.item.length, this.coats, cov)
    } else if (this.item.category.unit === 'Hr') {
      itemMaterial = this.item.material_quantity * this.item.quantity
    }

    this.item.material_quantity = Math.round(itemMaterial * 100) / 100
  }

  onRateChange(selectedRate: Rate) {
    this.item.rate = selectedRate
    this.setHours()
    this.setMaterialQuantity()
  }

  onCoatsChange(coats) {
    this.item.coats = coats.value
    this.setHours()
    this.setMaterialQuantity()
  }

  onPrepChange(prep) {
    this.item.prep = prep.value
    this.setHours()
  }

  onItemLengthChange(L) {
    this.setHours()
    this.setMaterialQuantity()
  }

  onItemWidthChange(W) {
    this.setHours()
    this.setMaterialQuantity()
  }

  onItemQuantityChange(Q) {
    this.setHours()
    this.setMaterialQuantity()
  }

  onMaterialQuantityChange(Q) {
    console.log('onMaterialQunatityChange')
    this.setMaterialQuantity()
  }

  onMaterialChange(selectedMaterial: Material) {
    this.item.material_type = selectedMaterial
    this.item.material_id = selectedMaterial.id
    this.setMaterialQuantity()
  }

  onSaveItem(form: NgForm) {
    this.item.set = true
    this.dismiss()
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }
}
