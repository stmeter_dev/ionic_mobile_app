import { Component, OnInit } from '@angular/core';
import { NavController, NavParams,
        ActionSheetController } from 'ionic-angular';
import { Takeoff } from '../../models/takeoff';
import { RoomPage } from './room/room';
import { Room } from '../../models/room';
import { TakeoffService } from '../../services/takeoff';
import { GlobalRate } from '../../models/global-rate';
import { ProdRate } from '../../models/prodrate';
import { FlexRate } from '../../models/flex-rate';
import { Measure } from '../../models/measure';
import { Material } from '../../models/material';

@Component({
  selector: 'page-interior-takeoff',
  templateUrl: 'interior-takeoff.html'
})
export class InteriorTakeoffPage implements OnInit {
  takeoff: Takeoff
  room: Room
  roomIndex: number = 0
  roomsList: Room[] = []
  jobMaterial: Material
  roomsCount: number = this.roomsList.length
  roomNames = {
    "Living Room": 0,
    "Dining Room": 0,
    "Bed Room": 0,
    "Bath Room": 0,
    "Kitchen": 0,
    "Laundry": 0
  }

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private asCtrl: ActionSheetController,
              private toService: TakeoffService
              ) {
  }

  ngOnInit() {
    this.takeoff = this.navParams.get('takeoff')
    this.roomsList = this.takeoff.rooms
    this.jobMaterial = this.toService.jobMaterial
    this.toService.materialChange.subscribe(
      (material: Material) => this.jobMaterial = material
    )
  }

  onAddRoom() {
    const actionSheet = this.asCtrl.create({
      title: 'Choose a room name',
      buttons: [
        {
          text: 'Living Room',
          handler: () => {
            this.addRoom('Living Room')
            this.navCtrl.push(RoomPage, {room: this.room})
          }
        },{
          text: 'Dining Room',
          handler: () => {
            this.addRoom('Dining Room')
            this.navCtrl.push(RoomPage, {room: this.room})
          }
        },
         {
          text: 'Bed Room',
          handler: () => {
            this.addRoom('Bed Room')
            this.navCtrl.push(RoomPage, {room: this.room})
          }
        }, {
          text: 'Bath Room',
          handler: () => {
            this.addRoom('Bath Room')
            this.navCtrl.push(RoomPage, {room: this.room})
          }
        }, {
          text: 'Kitchen',
          handler: () => {
            this.addRoom('Kitchen')
            this.navCtrl.push(RoomPage, {room: this.room})
          }
        }, {
          text: 'Laundry',
          handler: () => {
            this.addRoom('Laundry')
            this.navCtrl.push(RoomPage, {room: this.room})
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    })
    actionSheet.present()
  }

  onLoadRoom(room: Room, index: number) {
    let selectedRoom = this.roomsList.find((r: Room) => {
      return r.name === room.name
    })
    this.navCtrl.push(RoomPage, {room: selectedRoom})
  }

  setRoomName(name: string, count: number) {
    if (count <= 1) {
        return name
      } else {
        return name + ' ' + count
    }
  }

  addRoom(key: string) {
    this.roomNames[key]++
    const number = this.roomNames[key]
    const roomName = this.setRoomName(key, this.roomNames[key])
    this.room = new Room()
    this.room.index = this.roomIndex
    this.roomIndex++
    this.room.name = roomName
    this.room.number = number
    this.room.type = key
    this.room.material_type = this.jobMaterial
    console.log('room materialType ID ', this.room.material_id)
    //this.room = new Room(roomName, number, false, 10, 10, 10, key, 0, 0)
    this.roomsList.push(this.room)
    this.takeoff.rooms = this.roomsList

  }

  onDeleteRoom(index: number) {
    //this.toService.removeRoom(index)
    let room = this.roomsList.splice(index, 1).pop()
    this.roomNames[room.type]--
  }

}
