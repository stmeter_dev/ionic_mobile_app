import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AppStore } from '../../providers/app-store';
import { TakeoffService } from '../../services/takeoff';
import { EstimatesService } from '../../services/estimates';
import { Client } from '../../models/client';
import { Estimate } from '../../models/estimate';
import { Takeoff } from '../../models/takeoff';
import { Room } from '../../models/room';
import { GlobalRate } from '../../models/global-rate';
import { TakeoffSummaryPage } from '../takeoff-summary/takeoff-summary';
import { InteriorTakeoffPage } from '../interior-takeoff/interior-takeoff';
import { InteriorPreviewPage } from '../interior-preview/interior-preview';
import { ExteriorTakeoffPage } from '../exterior-takeoff/exterior-takeoff';

@Component({
  selector: 'page-takeoff',
  templateUrl: 'takeoff.html'
})
export class TakeoffPage implements OnInit {
  estimate: Estimate
  jobType: string
  client: Client
  takeoff: Takeoff
  gRates: GlobalRate[] = []
  propertyId: number

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private alrtCtrl: AlertController,
              private store: AppStore,
              private toService: TakeoffService,
              private estimatesService: EstimatesService
              ) {
  }

  ngOnInit() {
    //this.toService.fetchAll()
    this.estimate = this.navParams.get('estimate')
    this.takeoff = this.estimate.takeoff
    this.jobType = this.estimate.work_type
    this.client = this.estimate.property.client
    this.gRates = this.store.findItemByName('globalRates').data
    this.propertyId = this.estimate.property.id

  }

  ionViewWillEnter() {
    this.setWorkHours()
    this.setJobLaborCost()
    this.setJobMaterial()
    this.setJobMaterialsCost()
    this.setJobTotalCost()
    this.setJobProfit()
    this.setJobTax()
    this.setJobPrice()
    console.log(this.takeoff.status)
    console.log('rooms ',this.takeoff.rooms.length)
  }

  onLoadInterior() {
    if(this.takeoff.status === 'Ongoing') {
      this.navCtrl.push(InteriorTakeoffPage, { takeoff: this.takeoff })
    } else {
      this.navCtrl.push(InteriorPreviewPage, { takeoff: this.takeoff })
    }
  }

  onLoadExterior() {
    this.navCtrl.push(ExteriorTakeoffPage)
  }

  onPreviewSummary() {
    this.navCtrl.push(TakeoffSummaryPage, {jobType: this.jobType, takeoff: this.takeoff})
  }

  onStartInclusions() {
    //this.navCtrl.push(NewEstimatePage)
  }

  onStartExclusions() {
    //this.navCtrl.push(NewEstimatePage)
  }

  onStartComments() {
    //this.navCtrl.push(NewEstimatePage)
  }

  onStartSignature() {
    //this.navCtrl.push(NewEstimatePage)
  }

  filterRoomFeatures() {
    //let rooms = this.takeoff.rooms
    this.takeoff.rooms.forEach(room => {
      let filteredRoomFeatures = room.room_features_attributes.filter(item => {
        return item.hours > 0
      })
      room.room_features_attributes = filteredRoomFeatures
    })
    console.log('filtered rooms features ',this.takeoff.rooms)

  }

  setRoomsTakeoffId(toId: number) {
    this.takeoff.rooms.forEach((room: Room) => {
      room.takeoff_id = toId
    })
  }

  ProccedWithCompletion() {
    //this.setRooms()
    this.takeoff.status = 'Completed'
    this.takeoff.end = new Date()
    this.store.estimates.push(this.estimate)
    this.store.addItem({name: 'estimates', data: this.store.estimates})
    console.log('takeoff rooms',this.takeoff.rooms)
    this.filterRoomFeatures()
    this.toService.createTakeoff(this.estimate.id, this.takeoff)
    .subscribe(
      (takeoff: Takeoff) => {
        console.log('saved takeoff: ', takeoff)
        this.takeoff.id = takeoff.id
        this.takeoff.estimate_id = takeoff.estimate_id
        this.store.addItem({name: 'takeoff', data: takeoff})
        this.estimate.status = 'Takeoff Completed'
        this.estimate.end_time = new Date()
        //update estimate status in api
        this.estimatesService.updateEstimate(this.estimate.id , this.estimate)
        .subscribe()
        //set rooms takeoff_id
        this.setRoomsTakeoffId(takeoff.id)
        //save rooms to property
        this.toService.createRooms(this.propertyId, this.takeoff.rooms)
        .subscribe(
          (rooms: Room[]) => {
            console.log(rooms)
          }
        )
      }
    )
    this.navCtrl.pop()
  }

  onCompleteTakeoff() {
    let confirmCompletion = this.alrtCtrl.create({
      title: 'Are you sure?',
      message: 'Are you sure to proceed with takeoff completion?',
      buttons: [
        {
          text: 'No, Wait',
          handler: () => {}
        }, {
          text: 'Yes',
          handler: () => {
            this.ProccedWithCompletion()
          }
        }
      ]
    })

    confirmCompletion.present();
  }

  setWorkHours() {
    let jobHours = this.takeoff.rooms.reduce((workHours: number, room: Room) => {
      return workHours + room.hours
    }, 0)
    this.takeoff.work_hours = jobHours
  }

  setJobLaborCost() {
    let jobLaborCost = this.takeoff.rooms.reduce((laborCost: number, room: Room) => {
      console.log('room labor cost',room.labor_cost)
      return laborCost + room.labor_cost
    }, 0)
    this.takeoff.job_labor_cost = jobLaborCost
  }

  setJobMaterial() {
    this.takeoff.job_materials = this.takeoff.rooms.reduce((jobMaterial: number, room: Room) => {
      return jobMaterial + room.material_quantity
    }, 0)
    console.log('setJobMaterial ', this.takeoff.job_materials)
  }

  setJobMaterialsCost() {
    this.takeoff.job_material_cost = this.takeoff.rooms.reduce((mCost: number, room: Room) => {
      return mCost + room.material_cost
    }, 0)
    //console.log('setJobMaterialsCost ', this.takeoff.jobMaterialsCost)
  }

  setJobProfit() {
    console.log('setJobProfit called')
    let profitRate: GlobalRate = this.gRates.find((rate: GlobalRate) => {
      return rate.name === 'profit'
    })
    let profitPercent = profitRate.value
    this.takeoff.job_profit = this.takeoff.job_total_cost * profitPercent / 100
    //console.log('profit ', this.takeoff.job_profit)
  }

  setJobTax() {
    console.log('setJobTax called')
    let taxRate: GlobalRate = this.gRates.find((rate: GlobalRate) => {
      return rate.name === 'tax'
    })
    let taxPercent = taxRate.value
    this.takeoff.job_tax = this.takeoff.job_labor_cost * taxPercent / 100
    //console.log('tax ', this.takeoff.job_tax)
  }

  setJobTotalCost() {
    this.takeoff.job_total_cost = this.takeoff.job_labor_cost + this.takeoff.job_material_cost
  }

  setJobPrice() {
    this.takeoff.job_price = Math.round((this.takeoff.job_total_cost + this.takeoff.job_tax +
                            this.takeoff.job_profit) * 100) / 100
  }


}
