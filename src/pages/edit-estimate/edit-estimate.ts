import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { NavController, NavParams } from 'ionic-angular';
import { Estimate } from '../../models/estimate';
import { Property } from '../../models/property';

@Component({
  selector: 'page-edit-estimate',
  templateUrl: 'edit-estimate.html'
})
export class EditEstimatePage implements OnInit {
  estimateParams: any
  estimate: Estimate;
  index: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    ) {
  }

  ngOnInit() {
    this.estimate = this.navParams.get('estimate')
    this.index = this.navParams.get('index')
  }

  onSaveEstimate(form: NgForm){
    this.estimateParams = {
      start_time: form.value.startDate,
      job_type: form.value.jobType,
      work_description: form.value.workDescription
    }
    //this.estimate = new Estimate(this.estimateParams)
  }

}
