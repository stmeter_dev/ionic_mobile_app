import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController, NavController } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { AppStore } from '../../providers/app-store';
import { User } from '../../models/user';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})

export class SignInPage {
  current_user: User
  constructor(
    private authService: AuthService,
    private store: AppStore,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navCtrl: NavController
  ) {}

  onSignin(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Signing you in...'
    })
    loading.present()
    // old version of handling signin
    // this.authService.signin(form.value.email, form.value.password)
    // .then(data => {
    //   loading.dismiss()
    //   this.navCtrl.push(TabsPage)
    // })
    // .catch(error => {
    //   loading.dismiss()
    //   const alert = this.alertCtrl.create({
    //     title: 'Signin Failed!',
    //     message: error.json().errors,
    //     buttons: ['OK']
    //   })
    //   alert.present()
    // })

    //new verion of handling signin using observable

    this.authService.signin(form.value.email, form.value.password)
    .subscribe(
      (user: User) => {
        loading.dismiss()
        this.current_user = user
        this.authService.userState.next(user)
        this.store.addItem({name: 'current_user', data: this.current_user})
        this.setHeaders()
        this.navCtrl.push(TabsPage)
      },
      (error: Error) => {
        console.log(error)
        loading.dismiss()
        const alert = this.alertCtrl.create({
          title: 'Signin Failed!',
          message: error.message,
          buttons: ['OK']
        })
        alert.present()
      }
    )
  }

  setHeaders() {
    this.store.headers.set('Authorization', this.current_user.authToken)
    this.store.headers.set('X-Account', this.current_user.account)
  }
}
