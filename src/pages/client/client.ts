import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Client } from '../../models/client';
import { Estimate } from '../../models/estimate';
import { EditClientPage } from '../edit-client/edit-client';

@Component({
  selector: 'page-client',
  templateUrl: 'client.html'
})
export class ClientPage implements OnInit {
  client: Client;
  index: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              ) {
  }

  ngOnInit() {
    this.client = this.navParams.get('client')
    this.index = this.navParams.get('index')
  }

  onEditClient() {
    console.log(this.client)
    this.navCtrl.push(EditClientPage, {client: this.client, index: this.index})
  }

  onDeleteClient() {
    //this.recipesService.removeRecipe(this.index);
    this.navCtrl.popToRoot();
  }
}
