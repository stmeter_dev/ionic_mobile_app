import { Component, OnInit } from '@angular/core';
import { NavController,
  LoadingController,
  AlertController,
  ActionSheetController } from 'ionic-angular';
import { Estimate } from '../../models/estimate';
import { EstimatePage } from '../estimate/estimate';
import { NewEstimatePage } from '../new-estimate/new-estimate';
import { EstimatesService } from '../../services/estimates';
import { AuthService } from '../../services/auth';
import { Observable } from 'rxjs/Observable';
import { TakeoffPage } from '../takeoff/takeoff';
import { SignInPage } from '../signin/signin';
import { User } from '../../models/user';

@Component({
  selector: 'page-estimates',
  templateUrl: 'estimates.html'
})

export class EstimatesPage implements OnInit{
  estimatePage:any = EstimatePage
  estimates: Estimate[] = []
  newEstimatePage:any = NewEstimatePage

  constructor(
    private navCtrl: NavController,
    private estimatesService: EstimatesService,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private asCtrl: ActionSheetController
  ) {

  }

  ionViewCanEnter() {
    this.authService.userState.subscribe(
      (user: User) => {
        if(user) {
          return true
        } else {
          return false
        }
      }
    )
  }

  onLoadEstimate(estimate: Estimate) {
    console.log(estimate)
    this.navCtrl.push(EstimatePage, {estimate: estimate})
  }

  onAddEstimate() {
    this.navCtrl.push(this.newEstimatePage)
  }

  onDeleteEstimate(estimate: Estimate, index: number) {
    const deleteAlert = this.alertCtrl.create({
          title: 'Are you sure?',
          message: 'Are you sure you wanna delete this estimate?',
          buttons: [
            {
              text: 'Keep it',
              role: 'cancel',
              handler: () => {}
            },
            {
              text: 'Yes, delete it',
              handler: () => {
                this.estimatesService.removeEstimate(estimate, index)
                .subscribe(
                  () => {},
                  (error: Error) => {
                    const alert = this.alertCtrl.create({
                      title: 'Deleting Estimate Failed!',
                      buttons: ['OK']
                    })
                    alert.present()
                  })
                }
              }
            ]
        })

        deleteAlert.present()
  }

  // onDeleteEstimate(estimate: Estimate) {
  //   const deleteAlert = this.alertCtrl.create({
  //         title: 'Are you sure?',
  //         message: 'Are you sure you wanna delete this estimate?',
  //         buttons: [
  //           {
  //             text: 'Keep it',
  //             role: 'cancel',
  //             handler: () => {}
  //           },
  //           {
  //             text: 'Yes, delete it',
  //             handler: () => {
  //               this.estimatesService.removeEstimate(estimate)
  //               .then(resp => {
  //                 const index = this.estimatesService.estimates.findIndex((el: Estimate) => {
  //                   return el.id === estimate.id
  //                 })
  //                 this.estimatesService.estimates.splice(index, 1)
  //                 this.estimates = this.estimatesService.getEstimates()
  //               })
  //               .catch(error => {
  //                 const alert = this.alertCtrl.create({
  //                   title: 'Deleting Estimate Failed!',
  //                   buttons: ['OK']
  //                 })
  //                 alert.present()
  //               })
  //             }
  //           }
  //         ]
  //       })
  //   deleteAlert.present()
  // }

  // ngOnInit() {
  //   //console.log('ionViewWillEnter')
  //   const loading = this.loadingCtrl.create({
  //     content: 'Loading estimates...'
  //   })
  //   loading.present()
  //   this.authService.getCurrentUser()
  //   .then(user => {
  //       if(user) {
  //         this.estimatesService.fetchEstimates(user)
  //         .then((resp) => {
  //           loading.dismiss()
  //           const eList = []
  //           resp.json().estimates.forEach(estimate => {
  //             eList.push(new Estimate(estimate))
  //           })
  //           if(eList) {
  //             this.estimatesService.estimates = eList
  //             this.estimates = this.estimatesService.getEstimates()
  //           } else {
  //             this.estimatesService.estimates = []
  //           }
  //         })
  //         .catch(error => {
  //           console.log(error)
  //           loading.dismiss()
  //           const alert = this.alertCtrl.create({
  //               title: 'Ooops!',
  //               message: 'Please signin first',
  //               buttons: ['OK']
  //             })
  //             alert.present()
  //         })
  //       } else {
  //         loading.dismiss()
  //         this.navCtrl.push(SignInPage)
  //       }
  //     }
  //   )
  //
  // }

  ngOnInit() {
    this.estimatesService.estimatesState.subscribe(
      (estimates: Estimate[]) => {
        this.estimates = estimates
      }
    )

    const loading = this.loadingCtrl.create({
      content: 'Loading estimates...'
    })
    loading.present()
    console.log('will load estimates --estimates ')
    this.estimatesService.fetchEstimates().subscribe(
      (estimates: Estimate[]) => {
        loading.dismiss()
        this.estimatesService.estimates = estimates
        this.estimates = estimates
        console.log(estimates)
      },
      (error: Error) => {
        loading.dismiss()
        const alert = this.alertCtrl.create({
          title: 'Ooops!',
          message: 'Please signin first',
          buttons: ['OK']
        })
        alert.present()
        this.navCtrl.push(SignInPage)
      }
    )
  }

  onStartTakeoff(estimate: Estimate) {
    const actionSheet = this.asCtrl.create({
      title: 'Choose Estimating Method',
      buttons: [
        {
          text: 'Flex Estimating',
          handler: () => {
            this.navCtrl.push(TakeoffPage, {mode: 'flex', estimate: estimate})
          }
        }, {
          text: 'Prod Rates Estimating',
          handler: () => {
            this.navCtrl.push(TakeoffPage, {mode: 'prodrates', estimate: estimate})
          }
        }
      ]
    })
    actionSheet.present()
  }
}
