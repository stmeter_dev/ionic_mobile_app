import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TakeoffService } from '../../services/takeoff';
import { Side } from '../../models/side';
import { SidePage } from './side/side';

@Component({
  selector: 'page-exterior-takeoff',
  templateUrl: 'exterior-takeoff.html'
})
export class ExteriorTakeoffPage implements OnInit {
  sidesList: Side[] = []
  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private toService: TakeoffService
              ) {
  }

  ngOnInit() {
    this.toService.setSides()
    this.sidesList = this.toService.getSides()
  }

  onLoadSide(side: Side) {
    this.navCtrl.push(SidePage, {side: side})
  }

}
