import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Side } from '../../../models/side';

@Component({
  selector: 'page-side',
  templateUrl: 'side.html'
})
export class SidePage implements OnInit{
  side: Side
  constructor(private navCtrl: NavController,
              private navParams: NavParams
              ) {
  }

  ngOnInit() {
    this.side = this.navParams.get('side')
    console.log(this.side)
  }

  onAddSide() {
    //this.navCtrl.push(NewEstimatePage)
  } 
}
