import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from 'ionic-angular';
import { Room } from '../../../models/room';
import { Item } from '../../../models/item';
import { ItemPreviewPage } from '../item-preview/item-preview';

@Component({
  selector: 'page-room-preveiw',
  templateUrl: 'room-preview.html'
})
export class RoomPreviewPage implements OnInit {
  room: Room
  items: Item[]

  roomItems: Item[] = []
  constructor(
    private navParams: NavParams,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.room = this.navParams.get('room')
    this.items = this.room.room_features_attributes
    console.log('items in room preview: ',this.items)
  }

  onLoadItem(item: Item) {
    let itemModal = this.modalCtrl.create(ItemPreviewPage, {item: item})
    itemModal.present()
  }

}
