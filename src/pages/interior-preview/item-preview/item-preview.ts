import { Component, OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Item } from '../../../models/item';

@Component({
  selector: 'page-item-preview',
  templateUrl: 'item-preview.html'
})
export class ItemPreviewPage implements OnInit {
  item: Item

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController
  ) {}

  ngOnInit() {
    this.item = this.navParams.get('item')
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
