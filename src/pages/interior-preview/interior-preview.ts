import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Takeoff } from '../../models/takeoff';
import { Room } from '../../models/room';
import { RoomPreviewPage } from './room-preview/room-preview';

@Component({
  selector: 'page-interior-preview',
  templateUrl: 'interior-preview.html'
})
export class InteriorPreviewPage implements OnInit {
  takeoff: Takeoff
  rooms: Room[]
  constructor(private navCtrl: NavController,
              private navParams: NavParams
              ) {
  }

  ngOnInit() {
    this.takeoff = this.navParams.get('takeoff')
    this.rooms = this.takeoff.rooms
  }

  ionViewWillEnter() {

  }

  onPreviewRoom(room: Room, index: number) {
    let selectedRoom = this.rooms.find((r: Room) => {
      return r.name === room.name
    })
    this.navCtrl.push(RoomPreviewPage, {room: selectedRoom})
  }


}
