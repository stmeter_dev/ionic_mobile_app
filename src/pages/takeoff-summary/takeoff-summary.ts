import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Takeoff } from '../../models/takeoff';


@Component({
  selector: 'page-takeoff-summary',
  templateUrl: 'takeoff-summary.html'
})
export class TakeoffSummaryPage implements OnInit {
  takeoff: Takeoff
  constructor(private navCtrl: NavController,
              private navParams: NavParams
              ) {
  }

  ngOnInit() {
    this.takeoff = this.navParams.get('takeoff')
  }


}
