import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ClientsPage } from '../clients/clients';
import { EstimatesPage } from '../estimates/estimates';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})

export class TabsPage {
  clientsPage:any = ClientsPage;
  estimatesPage:any = EstimatesPage;

  constructor(
    private navCtrl: NavController
  ) {

  }
}