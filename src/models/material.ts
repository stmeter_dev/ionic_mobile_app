export class Material {
  constructor(
    public name: string,
    public cov: number,
    public cost: number,
    public id?: number
  ) {}
}
