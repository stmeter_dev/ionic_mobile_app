export class Measure {
  constructor(
    public name: string,
    public value: number,
    public category: string,
    public unit: string,
    public id?: number
  ) {}
}
