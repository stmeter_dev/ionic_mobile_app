import { Client } from './client';
export class Property {
  addr_line1: string
  addr_line2: string
  zipcode: string
  city: string
  state: string
  address: string
  id?: number
  client?: Client
  constructor(data: any) {
    this.id = data.id
    this.addr_line1 = data.addr_line1
    this.addr_line2 = data.addr_line2
    this.zipcode = data.zipcode
    this.city = data.city
    this.state = data.state
    this.address = this.addr_line1
    //this.address = this.addr_line1 + ', ' + this.addr_line2
    this.client = new Client(data.client)
  }
}
