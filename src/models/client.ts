import { Property } from './property';

export class Client {
   first_name: string
   last_name: string
   phone: string
   cell: string
   email: string
   full_name: string
   properties?: Property[]
  constructor(data: any) {
    this.first_name = data.first_name
    this.last_name = data.last_name
    this.phone = data.phone_number
    this.cell = data.cell
    this.email = data.email
    this.full_name = this.first_name + ' ' + this.last_name
    this.properties = data.properties
  }
}