export class User {

  constructor(
    public name: string,
    public email: string,
    public account: string,
    public authToken: string,
    public isAuthenticated: boolean
  ) {}
}