import { Rate } from './rate';
import { RateCategory } from './rate-category';
import { Material } from './material';

export class Item {
  constructor(
    public name: string,
    public quantity: number = 0,
    public set: boolean,
    public hours: number,
    public coats: number,
    public prep: number,
    public material_quantity: number,
    public material_type: Material,
    public material_id: number,
    public length: number = 0,
    public width: number = 0,
    public comment: string = '',
    public pic_url: string = '',
    public category?: RateCategory,
    public rate_category_id?: number,
    public rate?: Rate,
    public rate_id?: number
  ) {}
}
