import { Room } from './room';
import { Side } from './side';

export class Takeoff {
  constructor (
    public start: Date = null,
    public end: Date = null,
    public status: string = 'Pending',
    public saved: boolean = false,
    public work_hours: number = 0,
    public job_labor_cost: number = 0,
    public job_materials: number = 0,
    public job_material_cost: number = 0,
    public job_total_cost: number = 0,
    public job_profit: number = 0,
    public job_tax: number = 0,
    public job_price: number = 0,
    public rooms: Room[] = [],
    public sides: Side[] = [],
    public id?: number,
    public estimate_id?: number
  ) {}
}

// takeoff status = ['Pending', 'Ongoing', 'Completed']
