export class ProdRate {
  constructor(
    public name: string,
    public rate: number,
    public unit: string,
    public category: string,
    public id?: number
  ) {}
}
