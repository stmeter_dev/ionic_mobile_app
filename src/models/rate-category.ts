import { Rate } from './rate';

export class RateCategory {
  constructor(
    public name: string, public unit: string, public inout: string,
    public rates?: Rate[], public id?: number
  ) {}
}
