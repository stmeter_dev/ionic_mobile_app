import { DateTime } from "ionic-angular";
import { Property } from './property';
import { Takeoff } from './takeoff';

export class Estimate {
  id: number
  start_time: Date
  end_time: Date
  work_description: string
  status: string
  work_type: string
  property: Property
  takeoff: Takeoff


  constructor(data: any) {
    this.id = data.id
    this.start_time = data.start_time
    this.work_description = data.work_description
    this.status = data.status
    this.work_type = data.work_type
    this.property = new Property(data.property)
    this.takeoff = new Takeoff()
  }
}
