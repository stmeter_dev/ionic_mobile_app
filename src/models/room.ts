import { Item } from './item';
import { Material } from './material';

export class Room {
  constructor (
    public index: number = 0,
    public name: string = '',
    public number: number = 0,
    public set: boolean = false,
    public width: number = 10,
    public length: number = 10,
    public height: number = 10,
    public type: string = '',
    public hours: number = 0,
    public material_quantity: number = 0,
    public labor_cost: number = 0,
    public material_cost: number = 0,
    public room_features_attributes: Item[] = [],
    public material_type: Material = null,
    public material_id: number = -1,
    public id?: number,
    public takeoff_id?: number
  ) {}
}
