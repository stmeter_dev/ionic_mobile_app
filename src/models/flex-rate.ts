export class FlexRate {
  constructor(
    public name: string, public description: string, public rate: number,
    public unit: string, public id?: number
  ) {}
}
